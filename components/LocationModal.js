import React from 'react'
import { AsyncStorage, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Icon, Card, CardItem } from 'native-base'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import Geolocation from '@react-native-community/geolocation'
import Geocoder from 'react-native-geocoder'
class LocationModal extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        recentLocations: []
      }
    }
    async componentWillMount() {
      let recentLocations = await AsyncStorage.getItem('recentLocations');
      recentLocations = JSON.parse(recentLocations);
      this.setState({ 
        recentLocations: recentLocations ? recentLocations : []
      })
    }
    current_location = async () => {
      Geolocation.getCurrentPosition((position) => {
          Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(async (res) => {
              let location = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                  loc: res[0].streetName
              };
              await AsyncStorage.setItem('location', JSON.stringify(location));
              await this.setRecentLocation(location);
              this.props.closeModal(location.loc.split(",")[0]);
              this.props.init_data();
          })
        }, err => {
            alert(JSON.stringify(err))
        }, { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      )
    }
    
    setLocation = async (details) => {
      let location = {
        lat: details.geometry.location.lat,
        lng: details.geometry.location.lng,
        loc: details.formatted_address
      }
      await AsyncStorage.setItem('location', JSON.stringify(location))
      await this.setRecentLocation(location)
      await this.props.closeModal(location.loc.split(",")[0]);
      this.props.init_data();
    }

    setOriginLocation = async (location) => {
      await AsyncStorage.setItem('location', JSON.stringify(location));
      await this.setRecentLocation(location);
      await this.props.closeModal(location.loc.split(",")[0])
      this.props.init_data();
    }

    setRecentLocation = async (location) => {
      let recentLocations = await AsyncStorage.getItem('recentLocation');
      recentLocations = recentLocations !== null ? JSON.parse(recentLocations) : [];
      if (recentLocations.length < 3) {
        recentLocations.unshift(location);
      } else {
        recentLocations.pop();
        recentLocations.unshift(location)
      }
      await AsyncStorage.setItem('recentLocations', JSON.stringify(recentLocations));
    }

    render() {
      const query = { key: 'AIzaSyCtlR4RO7TRNLFw0RjiXQncr9Nzu3cXbIk', language: 'en', types: 'geocode' }
      const geoLocation = {
        textInputContainer: {
          width: '100%',
          backgroundColor: '#ffffff',
          borderTopWidth: 1,
          borderRightWidth: 1,
          borderLeftWidth: 1,
          borderRadius: 10,
          borderColor: '#cccccc',
          borderTopColor: '#cccccc'
        },
        description: { fontWeight: 'bold' },
        predefinedPlacesDescription: { color: '#1faadb' }
      }
      return (
        <View style={styles.modalContainer}>
            <GooglePlacesAutocomplete
              placeholder='Please enter location' minLength={2}
              autoFocus={false} returnKeyType={'search'}
              listViewDisplayed='auto' fetchDetails={true}
              renderDescription={row => row.description} onPress={(data, details = null) => this.setLocation(details)}
              query={query} styles={geoLocation} currentLocation={false}
              debounce={200} nearbyPlacesAPI='GooglePlacesSearch' 
              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
              renderLeftButton={() => <Icon name="search" style={styles.searchIcon}/>}
            >
              {this.state.recentLocations.length ? <Text>Recent Location</Text> : <></>}
              <Card transparent>
                {this.state.recentLocations.map((location, idx) => (
                  <TouchableOpacity onPress={() => {this.setOriginLocation(location)}} key={idx}>
                    <CardItem>
                      <Icon name="locate" style={styles.locateIcon}></Icon>
                      <Text style={styles.locateColor}>{location.loc.split(',')[0]}</Text>
                    </CardItem>
                  </TouchableOpacity>
                ))}
              </Card>
            </GooglePlacesAutocomplete>
        </View>
      )
    }
    
}

const styles = StyleSheet.create({
  modalContainer: {width: '100%', height: '100%'},
  searchIcon: {
    paddingTop: 12, 
    paddingLeft: 12, 
    color: '#555555', 
    fontSize: 20
  },
  locateIcon: {fontSize: 20, color: '#ec5175'},
  locateColor: {color: '#ec5175'}
})
export default LocationModal