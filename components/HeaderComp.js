import React from 'react'
import { StyleSheet } from 'react-native'
import { Header, Left, Body, Title, Icon, Button } from 'native-base'
import { withNavigation } from 'react-navigation';

class HeaderComp extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let {back, title} = this.props;
        return (
            <Header style={styles.header_container}>
                <Left>
                    {back == "" ? <></> : 
                    <Button style={styles.header_btn} onPress={() => this.props.navigation.navigate(back)}>
                        <Icon name="arrow-back"/>
                    </Button>
                    }
                </Left>
                <Body>
                    <Title>{title}</Title>
                </Body>
            </Header>
        )
    }
}

export default withNavigation(HeaderComp);

const styles = StyleSheet.create({
    header_container: {
        height: 40
    },
    header_btn: {
        height: 40
    }
})