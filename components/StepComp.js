import React from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import { Text } from 'native-base'
import StepIndicator from 'react-native-step-indicator'

class StepComp extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let labels = [
            {title: "Order Placed", body: "We have received your order."}, 
            {title: "Order Confirmed", body: "Your order has been confirmed."}, 
            {title: "Order Processed", body: "We are preparing your order."}, 
            {title: "Ready to Pickup", body: "Your order is ready for pickup."}]
        let customStyles = {
            stepIndicatorSize: 25,
            currentStepIndicatorSize:30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: '#fe7013',
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: '#fe7013',
            stepStrokeUnFinishedColor: '#aaaaaa',
            separatorFinishedColor: '#fe7013',
            separatorUnFinishedColor: '#aaaaaa',
            stepIndicatorFinishedColor: '#fe7013',
            stepIndicatorUnFinishedColor: '#ffffff',
            stepIndicatorCurrentColor: '#ffffff',
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: '#fe7013',
            stepIndicatorLabelFinishedColor: '#ffffff',
            stepIndicatorLabelUnFinishedColor: '#aaaaaa',
            labelColor: '#999999',
            labelSize: 13,
            currentStepLabelColor: '#fe7013'
        }
        let { order_id, delivery_time, currentPage } = this.props
        return (
            <React.Fragment>
                <View style={styles.header_container}>
                    <View style={styles.header_text}>
                        <Text style={styles.title}>ESTIMATED TIME</Text>
                        <Text style={styles.text}>{delivery_time} Minutes</Text>
                    </View>
                    <View style={styles.header_text}>
                        <Text style={styles.title}>ORDER NUMBER</Text>
                        <Text>{order_id}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={[styles.timeline]}>
                        <StepIndicator
                            customStyles={customStyles}
                            stepCount={4}
                            currentPosition={currentPage}
                            direction={'vertical'}
                        />
                    </View>
                    <View style={styles.timeline_body}>
                        {labels.map((item, idx) => (
                            <View style={{height: (Dimensions.get('screen').height - 260)/4, justifyContent: 'center'}}>
                                <Text style={styles.title}>{item.title}</Text>
                                <Text style={styles.text}>{item.body}</Text>
                            </View>
                        ))}
                    </View>
                </View>
            </React.Fragment>
        )
    }
}

export default StepComp

const styles = StyleSheet.create({
    header_container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 10,
        backgroundColor: '#eeeeee',
        borderBottomWidth: 0.5,
        borderBottomColor: '#cccccc'
    },
    header_text: {
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold'
    },
    text : {},
    timeline: {
        height: Dimensions.get('screen').height - 220,
        padding: 20,
        flex: 1
    },
    timeline_body: {
        flex: 10, 
        justifyContent: 'center',
        height: Dimensions.get('screen').height - 220
    }
})