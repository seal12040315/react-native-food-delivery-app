import React from 'react'
import {Footer, FooterTab, Icon, Button} from 'native-base'
import { withNavigation } from 'react-navigation';

class FooterComp extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Footer style={{height: 40}}>
                <FooterTab>
                    <Button vertical onPress={() => this.props.navigation.navigate('HomeScreen')}>
                        <Icon name="home"/>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('OrderScreen')}>
                        <Icon name="ios-list"/>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('ProfileScreen')}>
                        <Icon name="ios-contact"/>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('WalletScreen')}>
                        <Icon name="ios-cash"/>
                    </Button>
                    <Button vertical onPress={() => this.props.navigation.navigate('MyAddressScreen')}>
                        <Icon name="ios-create"/>
                    </Button>
                    {this.props.navigation.state.routeName === 'HomeScreen' ? <React.Fragment></React.Fragment> :
                    <Button vertical onPress={() => this.props.navigation.navigate('TrackOrderScreen')}>
                        <Icon name="bookmark"/>
                    </Button>}
                </FooterTab>
            </Footer>
        )
    }
}

export default withNavigation(FooterComp);