import * as Actions from '../actions'

const initialState = []
export default (state = initialState, action) => {
    switch(action.type) {
        case Actions.CHOOSE_RESTAURANT_DATA:
            return action.payload
        default: 
            return state
    }
}