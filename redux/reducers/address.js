import * as Actions from '../actions'

const initialState = {
    addresses: []
}
export default (state = initialState, action) => {
    switch(action.type) {
        case Actions.SET_ADDRESSES:
            return {addresses: action.payload}
        default:
            return state
    }
}