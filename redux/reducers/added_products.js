import * as Actions from '../actions'

const initialState = {
    added_products: []
}
export default (state = initialState, action) => {
    switch(action.type) {
        case Actions.SET_ADDED_PRODUCT:
            return {added_products: action.payload}
        default:
            return state
    }
}