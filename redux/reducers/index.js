import { combineReducers } from 'redux'
import SpinnerReducer from './spinner'
import UserReducer from './user'
import CategoriesReducer from './categories'
import Restaurant from './restaurant'
import RestaurantsReducer from './restaurants'
import AddedProductsReducer from './added_products'
import AddressReducer from './address'
export default combineReducers({
    spinner: SpinnerReducer,
    user: UserReducer,
    categories: CategoriesReducer,
    restaurant: Restaurant,
    restaurants: RestaurantsReducer,
    added_products: AddedProductsReducer,
    addresses: AddressReducer
})