export const SET_CATEGORIES_DATA = 'SET_CATEGIRIES_DATA'
export const setCategoriesData = (categories) => {
    return (dispatch) => {
        dispatch({
            type: SET_CATEGORIES_DATA,
            payload: categories
        })
    }
}