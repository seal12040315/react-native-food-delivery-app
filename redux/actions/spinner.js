export const SET_SPINNER_DATA = 'SET_SPINNER_DATA'
export const setSpinnerData = (spinner) => {
    return (dispatch) => {
        dispatch({
            type: SET_SPINNER_DATA,
            payload: spinner
        })
    }
}