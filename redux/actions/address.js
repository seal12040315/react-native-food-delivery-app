export const SET_ADDRESSES = 'SET_ADDRESSES'

export const setAddresses = (addresses) => {
    return (dispatch) => {
        dispatch({
            type: SET_ADDRESSES,
            payload: addresses
        })
    }
}