export const CHOOSE_RESTAURANT_DATA = 'CHOOSE_RESTAURANT_DATA'
export const chooseRestaurantData = (restautrant) => {
    return (dispatch) => {
        dispatch({
            type: CHOOSE_RESTAURANT_DATA,
            payload: restautrant
        })
    }
}