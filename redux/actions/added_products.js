export const SET_ADDED_PRODUCT = 'SET_ADDED_PRODUCT'

export const setAddedProductsData = (products) => {
    return (dispatch) => {
        dispatch({
            type: SET_ADDED_PRODUCT,
            payload: products
        })
    }
}