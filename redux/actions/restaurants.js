export const SET_RESTAURANTS_DATA = 'SET_RESTAURANTS_DATA'
export const setRestaurantsData = (restautrants) => {
    return (dispatch) => {
        dispatch({
            type: SET_RESTAURANTS_DATA,
            payload: restautrants
        })
    }
}