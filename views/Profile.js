import React from 'react'
import { StyleSheet, AsyncStorage, View } from 'react-native'
import { Container, Header, Content, Toast, Item, Input, Alert, Title, Text, Left, Body, Right, List, ListItem, Button, Thumbnail, Icon } from 'native-base'
import { config } from '../config'
import Axios from 'axios';
import FooterComp from '../components/FooterComp'
import Modal from 'react-native-modal'

class Profile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            editModal: false,
            old_password: "",
            new_password: ""
        }
    }

    async componentDidMount() {
        let userString = await AsyncStorage.getItem('user')
        this.setState({
            user: JSON.parse(userString)
        })
    }

    change_input = (key, event) => {
        let user = {...this.state.user, [key]: event.nativeEvent.text}
        this.setState({
            user: user
        })
    }

    changeInput = (key, event) => {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    updateProfile = () => {
        this.setState({
            editModal: false
        })
        let params = {
            user_id: this.state.user.user_id,
            name: this.state.user.name,
            mobile: this.state.user.mobile
        }
        Axios.post(`${config.baseUrl}/update_profile`, params).then( async res => {
            await AsyncStorage.setItem('user', JSON.stringify(this.state.user))
            this.setState({
                name_disabled: true,
                mobile_disabled: true
            })
            Toast.show({
                text: res.data.message,
                buttonText: "Ok",
                type: "success",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        }, err => {
            Toast.show({
                text: JSON.stringify(err),
                buttonText: "Ok",
                type: "danger",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        })
    }

    changePassword = () => {
        let params = {
            user_id: this.state.user.user_id,
            old_password: this.state.old_password,
            new_password: this.state.new_password
        }
        Axios.post(`${config.baseUrl}/change_password`, params).then(res => {
            let {data} = res
            if (data.status) {
                Alert.alert('Success!', data.message)
            } else {
                Alert.alert('Warnning!', data.message)
            }
        }, err => {
            alert(JSON.stringify(err))
        })
    }

    logout = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('LocationScreen')
    }
    
    render() {
        return (
            <Container>
                <Header style={{height: 40}}>
                    <Left>
                        <Button style={{height: 40}} onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body><Title>PROFILE</Title></Body>
                    <Right>
                        <Button style={{height: 40}} onPress={() => this.logout()}>
                            <Icon name="log-out"/>
                        </Button>
                    </Right>
                </Header>
                <Content style={{padding: 15}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style={styles.Label}>Details</Text>
                        <Button rounded small style={{width: 25, height: 25, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.setState({editModal: true})}><Icon name="create" resizeMode="stretch" style={{fontSize: 14, marginLeft:8, marginRight:8, marginTop: 5, marginBottom: 5}}/></Button>
                    </View>
                    <List>
                        <ListItem style={styles.profileList}>
                            <Left style={{flex: 1}}>
                                <Thumbnail square resizeMode="contain" style={styles.Thumbnail} source={require('../assets/images/user.png')}/>
                            </Left>
                            <Body style={{flex: 6}}>
                                <Input disabled style={{height: 20, fontSize: 12, paddingTop: 0, paddingBottom: 0}} value={this.state.user.name}/>
                            </Body>
                        </ListItem>
                        <ListItem style={styles.profileList}>
                            <Left style={{flex: 1}}>
                                <Thumbnail square resizeMode="contain" style={styles.Thumbnail} source={require('../assets/images/email.png')}/>
                            </Left>
                            <Body style={{flex: 6}}>
                                <Text style={{fontSize: 12}}>{this.state.user.email}</Text>
                            </Body>
                        </ListItem>
                        <ListItem style={styles.profileList}>
                            <Left style={{flex: 1}}>
                                <Thumbnail square resizeMode="contain" style={styles.Thumbnail} source={require('../assets/images/phone.png')}/>
                            </Left>
                            <Body style={{flex: 6}}>
                                <Input ref="mobile" disabled style={{height: 20, fontSize: 12, paddingTop: 0, paddingBottom: 0}} value={this.state.user.mobile}/>
                            </Body>
                        </ListItem>
                    </List>
                    <Text style={styles.Label}>Change Password</Text>
                    <View style={[styles.changeForm]}>
                        <Item floatingLabel>
                            <Input placeholder="Old Password" style={{fontSize: 12}} value={this.state.password} onChange={(event) => this.changeInput('old_password', event)}/>
                        </Item>
                        <Item floatingLabel style={styles.changeFormInput}>
                            <Input placeholder="New Password" style={{fontSize: 12}} value={this.state.new_password} onChange={(event) => this.changeInput('new_password', event)}/>
                        </Item>
                        <View style={styles.rightButton}>
                            <Button small onPress={() => this.changePassword()}>
                                <Text style={{fontSize: 12}}>Change</Text>
                            </Button>
                        </View>
                    </View>
                </Content>
                <FooterComp/>
                <Modal isVisible={this.state.editModal} style={{margin: 15}}>
                    <View style={{backgroundColor: '#ffffff', padding: 15}}>
                        <Item>
                            <Input placeholder="Name" value={this.state.user.name} onChange={(event)=>{this.change_input('name', event)}}/>
                        </Item>
                        <Item>
                            <Input placeholder="Mobile" value={this.state.user.mobile} onChange={(event) => {this.change_input('mobile', event)}}/>
                        </Item>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, alignItems: 'center'}}>
                            <Button small danger onPress={() => this.setState({editModal: false})}><Text>Cancel</Text></Button>
                            <Button small onPress={() => {this.updateProfile()}}><Text>UPDATE</Text></Button>
                        </View>
                    </View>
                </Modal>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    Thumbnail: {
        height: 20
    },
    Label: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 15,
        marginBottom: 5
    },
    profileList: {
        paddingLeft: 0, 
        marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    changeForm: {
        paddingLeft: 20,
    },
    changeFormInput: {
        fontSize: 14
    },
    rightButton: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 15
    }
})

export default Profile