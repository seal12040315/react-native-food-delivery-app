import React from 'react'
import { StyleSheet, AsyncStorage, ImageBackground, Image, View, Alert } from 'react-native'
import { Container, Content, Item, Input, Icon, Button, Footer, FooterTab, Text } from 'native-base'
import Axios from 'axios';
import { config } from '../config' 
class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: ""
        }
    }

    changeInput(key, event) {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    loginHandle() {
        let params = {
            username: this.state.email,
            password: this.state.password
        }
        Axios.post(`${config.baseUrl}/login`, params).then(async (res) => {
            let { data } = res
            if (data.status) {
                await AsyncStorage.setItem('user', JSON.stringify(data.response));
                this.props.navigation.navigate('HomeScreen');
            } else {
                Alert.alert('Warnning!', data.message)
            }
        }, err => {

        })
    }

    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground source={require('../assets/images/background.png')} resizeMode="stretch" style={styles.bgImage}>
                        <Image source={require('../assets/images/logo.png')} style={styles.logo}/>
                    </ImageBackground>
                    <View style={styles.formContainer}>
                        <Item rounded style={styles.inputContainer}>
                            <Icon name="person"/>
                            <Input value={this.state.name} placeholder="Email" onChange={(event) => this.changeInput('email', event)}/>
                        </Item>
                        <Item rounded style={styles.inputContainer}>
                            <Icon name="lock"/>
                            <Input value={this.state.password} secureTextEntry={true} placeholder="Password" onChange={(event) => this.changeInput('password', event)}/>
                        </Item>
                        <View style={styles.buttonContainer}>
                            <Button large rounded warning style={styles.loginButton} onPress={() => this.loginHandle()}>
                                <Icon name="arrow-round-forward"/>
                            </Button>
                        </View>
                    </View>
                </Content>
                <Footer style={styles.transFooter}>
                    <FooterTab style={styles.transFooter}>
                        <Button transparent style={styles.forgotButton} onPress={() => this.props.navigation.navigate('ForgotScreen')}>
                            <Text style={{fontSize: 15}}>Forgot Password</Text>
                        </Button>
                        <Button transparent style={styles.registerButton} onPress={() => this.props.navigation.navigate('RegisterScreen')}>
                            <Text style={{fontSize: 15}}>Register</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bgImage: {
        width: '100%',
        height: 260,
    },
    logo: {
        width: 125,
        height: 120,
        marginTop: 65,
        marginLeft: 100
    },
    formContainer: {
        padding: 15
    }, 
    inputContainer: {
        padding: 8,
        height: 45,
        marginTop: 15,
        marginBottom: 15
    },
    buttonContainer: {
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    loginButton: {
        height: 70,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    transFooter: {
        backgroundColor: 'transparent',
        height: 40
    },
    forgotButton: {
        alignItems: 'flex-start'
    },
    registerButton: {
        alignItems: 'flex-end'
    }
})

export default Login
