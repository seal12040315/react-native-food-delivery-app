import React from 'react'
import {View, AsyncStorage, TouchableOpacity} from 'react-native'
import {Container, Card, CardItem, Icon, Item, Input , Content, Button, Text, Toast} from 'native-base'
import * as Actions from '../redux/actions'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import Axios from 'axios';
import {config} from '../config'

class MyAddress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user_id: "",
            toggle: false,
            name: "",
            mobile: "",
            address_1: "",
            address_2: "",
            addresses: []
        }
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user);
        this.setState({
            user_id: user.user_id
        })
        this.init_addresses()
    }

    init_addresses() {
        Axios.post(`${config.baseUrl}/my_addresses`, {user_id: this.state.user_id}).then(res => {
            this.setState({
                addresses: res.data.response
            })
        }, err => {
            alert(err)
        })
    }

    changeInput = (key, event) => {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    add_address = () => {
        let params = {
            user_id: this.state.user_id,
            name: this.state.name,
            mobile: this.state.mobile,
            address_1: this.state.address_1,
            address_2: this.state.address_2
        }
        Axios.post(`${config.baseUrl}/save_my_address`, params).then(res => {
            Toast.show({
                text: res.data.message,
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
            this.init_addresses();

        }, err => {
            Toast.show({
                text: "During adding new address, unexpected error.",
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        })
    }

    remove_address = (address_id) => {
        let params = {
            user_id: this.state.user_id,
            address_id: address_id
        }

        Axios.post(`${config.baseUrl}/delete_my_address`, params).then(res => {
            Toast.show({
                text: res.data.message,
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
            this.init_addresses();
        }, err => {
            Toast.show({
                text: "During removing the address, unexpected error.",
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        })
    }

    render() {
        return (
            <Container>
                <HeaderComp back="HomeScreen" title="MY ADDRESSES"/>
                <Content>
                    <TouchableOpacity onPress={() => this.setState({toggle: !this.state.toggle})}>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                            <Icon name="add" style={{margin: 10, fontSize: 20}}/>
                            <Text>Add new address</Text>
                        </View>
                    </TouchableOpacity>
                    {this.state.toggle ? 
                    <View style={{padding: 20, paddingTop: 0}}>
                        <Item>
                            <Input placeholder="Name *" value={this.state.name} onChange={(event) => this.changeInput('name', event)}/>
                        </Item>
                        <Item>
                            <Input placeholder="Mobile Number *" value={this.state.mobile} onChange={(event) => this.changeInput('mobile', event)}/>
                        </Item>
                        <Item>
                            <Input placeholder="Flat No. / House No. *" value={this.state.address_1} onChange={(event) => this.changeInput('address_1', event)}/>
                        </Item>
                        <Item>
                            <Input placeholder="Apartment / Locality name *" value={this.state.address_2} onChange={(event)=> this.changeInput('address_2', event)}/>
                        </Item>
                        <View style={{alignItems: 'flex-end', padding: 10, paddingRight: 0}}>
                            <Button small onPress={() => this.add_address()}><Text>Add New</Text></Button>
                        </View>
                    </View>
                    : <></>}
                    {this.state.addresses.map((address, idx) => (
                        <Card key={idx}>
                            <CardItem style={{marginBottom: 0, paddingBottom: 0, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Icon name="home"/>
                                    <Text>{address.name}</Text>
                                </View>
                                <Button transparent small onPress={() => this.remove_address(address.addresses_id)}><Text>Remove address</Text></Button>
                            </CardItem>
                            <CardItem style={{flexDirection: 'column', alignItems: 'flex-start', paddingBottom: 10, marginBottom: 0, paddingLeft: 20}}>
                                <Text>{address.mobile}</Text>
                                <Text>{address.address_1}</Text>
                                <Text>{address.address_2}</Text>
                            </CardItem>
                        </Card> 
                    ))}

                </Content>
                <FooterComp/>
            </Container>
        )
    }
}

mapStateToProps = (store) => {
    return {
        addresses: store.addresses.addresses
    }
}

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setAddresses: Actions.setAddresses, 
        removeAddress: Actions.removeAddress
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAddress)