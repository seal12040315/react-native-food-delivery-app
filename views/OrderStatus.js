import React from 'react'
import { BackHandler } from 'react-native'
import {Container, Content} from 'native-base'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import StepComp from '../components/StepComp'
import { connect } from 'react-redux'

class OrderStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order_id: "",
            currentPage: 0
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {return true})
        let order_uid = this.props.navigation.getParam('order_uid');
        let step = this.props.navigation.getParam('step');
        this.setState({
            order_id: order_uid,
            currentPage: step         
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', () => {return true})
    }

    render() {
        let {restaurant} = this.props
        return (
            <Container>
                <HeaderComp back="" title="ORDER STATUS"/>
                <Content>
                    <StepComp order_id={this.state.order_id} delivery_time={parseInt(restaurant.delivery_time)} currentPage={this.state.currentPage}/>
                </Content>
                <FooterComp/>
            </Container>
        )
    }
    
}

mapStateToProps = (state) => {
    return ({
        restaurant: state.restaurant
    })
}

export default connect(mapStateToProps, null)(OrderStatus)