import React from 'react'
import { AsyncStorage, View, StyleSheet, Alert, TouchableOpacity,ScrollView} from 'react-native'
import { Container, Left, Content, Button, Toast, Text, Footer, FooterTab, List, ListItem, Item, Input, Right, Radio, Icon, Spinner } from 'native-base'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NumericInput from 'react-native-numeric-input'
import Modal from 'react-native-modal'
import axios from 'axios'
import { config } from '../config'
import HeaderComp from '../components/HeaderComp'
import Axios from 'axios';
import RBSheet from 'react-native-raw-bottom-sheet'

class Checkout extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            new_address_1: "",
            new_address_2: "",
            spinner: false,
            addresses: [],
            total_amount: 0,
            service_tax: 0,
            vat: 0,
            price_after_tax: 0,
            delivery_charges: 0,
            packing_charges: 0,
            discount: 0,
            cashback: 0,
            amount_to_be_paid: 0,
            delivery_now_or_later: "",
            wallet: 0,
            products: [],
            location: "",
            coupon_code: "",
            delivery_address_id: "",
            restaurant: {},
            payment_type: 'COD',
            delivery_type: 'HOMEDELIVERY',
            clicked: 0
        }
    }

    async componentDidMount() {
        let location = await AsyncStorage.getItem('location');
        let restaurant = this.props.navigation.getParam('restaurant');
        let user = JSON.parse(await AsyncStorage.getItem('user'))
        let result = await axios.post(`${config.baseUrl}/my_addresses`, {user_id: user.user_id});
        this.setState({
            products: this.props.added_products.added_products,
            location: location.loc,
            restaurant: restaurant,
            addresses: result.data.response,
            delivery_address_id: result.data.response.length ? result.data.response[0].addresses_id : null,
            choosed_address: result.data.response.length ? result.data.response[0].address_1 + " " + result.data.response[0].address_2 : ""
        })
        this.cart_summary();
    }

    change_input = (key, event) => {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    addAddress = async () => {
        let user = JSON.parse(await AsyncStorage.getItem('user'));
        let params = {
            user_id: user.user_id,
            name: user.name,
            mobile: user.mobile,
            address_1: this.state.new_address_1,
            address_2: this.state.new_address_2
        }
        try {
            let result = await Axios.post(`${config.baseUrl}/save_my_address`, params);
            result = await axios.post(`${config.baseUrl}/my_addresses`, {user_id: user.user_id});
            this.setState({
                addresses: result.data.response,
                delivery_address_id: result.data.response.length ? result.data.response[0].addresses_id : null,
                choosed_address: result.data.response.length ? result.data.response[0].address_1 + " " + result.data.response[0].address_2 : "",
                addressModal: false
            })
        } catch (err) {
            Toast.show({
                text: "During adding new address, unexpected error.",
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        }
    }

    removeAddress = async (address_id) => {
        let user = JSON.parse(await AsyncStorage.getItem('user'))
        let params = {
            user_id: user.user_id,
            address_id: address_id
        }
        try {
            await Axios.post(`${config.baseUrl}/delete_my_address`, params);
            result = await axios.post(`${config.baseUrl}/my_addresses`, {user_id: user.user_id});
            this.setState({
                addresses: result.data.response               
            })          
        } catch (err){
            alert(err);
            Toast.show({
                text: "During removing the address, unexpected error.",
                buttonText: "Ok",
                type: "error",
                position: "top",
                style: {
                    margin: 10
                },
                duration: 3000
            })
        }
    }
    cart_summary = async () => {
        let user = JSON.parse(await AsyncStorage.getItem('user'));
        let params = {
            user_id: user.user_id,
            restaurant_id: this.state.restaurant.restaurant_id,
            delivery_location_id: this.state.location,
            coupon_code: this.state.coupon_code,
            delivery_type: this.state.delivery_type,
            items: this.state.products
        }
        Axios.post(`${config.baseUrl}/cart_summary`, params).then(res => {
            if (res.data.response.coupon_error_message) {
                Alert.alert('Warnning!', res.data.response.coupon_error_message);
            }
            if (res.data.response.coupon_success_message) {
                Alert.alert('Success!', res.data.response.coupon_success_message);
            }
            this.setState({
                total_amount: res.data.response.total_amount,
                service_tax: res.data.response.service_tax,
                vat: res.data.response.vat,
                price_after_tax: res.data.response.price_after_tax,
                delivery_charges: res.data.response.delivery_charges,
                packing_charges: res.data.response.packing_charges,
                discount: res.data.response.discount,
                cashback: res.data.response.cashback,
                amount_to_be_paid: res.data.response.amount_to_be_paid,
                delivery_now_or_laster: res.data.response.delivery_now_or_laster,
                wallet: res.data.response.wallet
            })
        })
    }

    add_product = (product, qty) => {
        let added_products = [...this.state.products];
        let isExist = added_products.findIndex(item => item.item_id === product.item_id);
        added_products[isExist].qty = qty            
        this.setState({
            products: added_products
        })
        this.cart_summary()
    }

    change_coupon_handle = (event) => {
        this.setState({
            coupon_code: event.nativeEvent.text
        });
    }

    choose_address = (address) => {
        this.setState({
            delivery_address_id: address.addresses_id,
            choosed_address: address.address_1 + ' ' + address.address_2
        });
        this.RBSheet.close();
    }

    get_uuid = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    check_out_handle = async () => {
        let user = await AsyncStorage.getItem('user');
        if (user) {
            user = JSON.parse(user);
            if (this.state.delivery_address_id === null) {
                this.setState({
                    addressModal: true
                })
            } else {
                if (this.state.total_amount !== 0 && this.state.clicked === 0) {
                    if (this.state.payment_type === 'COD') {
                        this.setState({
                            spinner: true,
                            clicked: 1
                        })
                        let params = {
                            user_id: user.user_id,
                            restaurant_id: this.state.restaurant.restaurant_id,
                            delivery_location_id: this.state.location,
                            delivery_type: 'HOMEDELIVERY',
                            delivery_address_id: this.state.delivery_address_id,
                            delivery_now_or_later: 'NOW',
                            coupon_code: this.state.coupon_code,
                            items: this.state.products,
                            payment_type: 'COD',
                            hellocash_used: false,
                            amount_to_be_paid: this.state.amount_to_be_paid,
                            cake_message: 'message'
                        }
                        try{
                            let {data} = await axios.post(`${config.baseUrl}/checkout`, params);
                            if (data.status) {
                                this.setState({
                                    total_amount: 0,
                                    service_tax: 0,
                                    vat: 0,
                                    price_after_tax: 0,
                                    delivery_charges: 0,
                                    packing_charges: 0,
                                    discount: 0,
                                    cashback: 0,
                                    amount_to_be_paid: 0,
                                    delivery_now_or_later: "",
                                    wallet: 0,
                                    products: [],
                                    location: "",
                                    coupon_code: "",
                                    delivery_address_id: "",
                                    restaurant: {},
                                    payment_type: 'COD',
                                    delivery_type: 'HOMEDELIVERY'
                                })
                                this.props.navigation.navigate('OrderStatusScreen', {order_uid: data.response.order_uid, step: 0});
                            } else {
                                Toast.show({
                                    text: data.message,
                                    buttonText: "Ok",
                                    type: "error",
                                    position: "top",
                                    style: {
                                        margin: 10
                                    },
                                    duration: 3000
                                })
                            }
                            this.setState({
                                clicked: 0,
                                spinner: false
                            })
                        } catch (err) {
                            Toast.show({
                                text: err,
                                buttonText: "Ok",
                                type: "error",
                                position: "top",
                                style: {
                                    margin: 10
                                },
                                duration: 3000
                            })
                            this.setState({
                                clicked: 0,
                                spinner: false
                            })
                        }
                    }
                }
            }
        } else {
            this.props.navigation.navigate('LoginScreen');
        }
    }
    render() {
        return (
            <Container>
                <HeaderComp back="ProductScreen" title="CHECKOUT"/>
                <Content style={styles.content}>
                    <Text style={styles.listTitle}>ITEMS</Text>
                    <View style={styles.block}>
                        <View styles={styles.block_content}>
                            <List>{
                                this.state.products.length ? this.state.products.map((product, index) => (
                                <ListItem style={{paddingTop: 5, marginLeft: 5, paddingBottom: 5, borderBottomWidth: 0}} key={index}>
                                    <View style={{flex: 6}}>
                                        <Text style={styles.product_name}>{product.name}</Text>
                                    </View>
                                    <View style={{flex: 2, alignItems: 'flex-start'}}>
                                        <Text note>₹{product.price * product.qty}</Text>
                                    </View>
                                    <View style={{flex: 1}}>
                                        <NumericInput value={product.qty} onChange={value => this.add_product(product, value)} iconSize={12} totalWidth={54} totalHeight={20} minValue={0} rounded={true}/>
                                    </View>
                                </ListItem>
                                )) : <ListItem></ListItem>
                            }</List>
                        </View>
                    </View>
                    <View style={styles.block}>
                        <Text style={styles.listTitle}>APPLY COUPON</Text>
                        <View style={styles.block_content}>
                            <Item rounded style={{borderRadius: 5}}>
                                <Input placeholder="Enter your coupon code..." style={{height: 30, paddingTop: 5, paddingBottom: 5, fontSize: 14}} value={this.state.coupon_code} onChange={(event) => this.change_coupon_handle(event)}/>
                                <Button small style={{marginRight: 5}} onPress={() => this.cart_summary()}><Text style={styles.listText}>Apply</Text></Button>
                            </Item>
                        </View>
                    </View>
                    <View style={styles.block}>
                        <Text style={styles.listTitle}>PRICES</Text>
                        <View style={styles.block_content}>
                            <View style={styles.price_item}>
                                <Left><Text note>Item Total</Text></Left>
                                <Right><Text note>₹{this.state.total_amount}</Text></Right>
                            </View>
                            <View style={styles.price_item}>
                                <Left><Text note>SGST</Text></Left>
                                <Right><Text note>{this.state.service_tax}</Text></Right>
                            </View>
                            <View style={styles.price_item}>
                                <Left><Text note>CGST</Text></Left>
                                <Right><Text note>{this.state.vat}</Text></Right>
                            </View>
                            <View style={styles.price_item}>
                                <Left><Text note>Packing Charges</Text></Left>
                                <Right><Text note>₹{this.state.packing_charges}</Text></Right>
                            </View>
                            <View style={styles.price_item}>
                                <Left><Text note>Delivery</Text></Left>
                                <Right><Text note>₹{this.state.delivery_charges}</Text></Right>
                            </View>
                            <View style={[styles.price_item]}>
                                <Left><Text style={styles.listText}>GRAND TOTAL</Text></Left>
                                <Right><Text style={styles.listText}>₹{this.state.amount_to_be_paid}</Text></Right>
                            </View>
                        </View>
                    </View>
                    <View style={styles.block}>
                        <Text style={styles.listTitle}>PAY USING</Text>
                        <View style={styles.block_content}>
                            <View style={styles.block_item}>
                                <Radio selected={this.state.payment_type == 'COD'} onPress={() => this.setState({payment_type: 'COD'})}/>
                                <Text style={{marginLeft: 10}}>Cash on delivery</Text>
                            </View>
                        </View>
                    </View>
                </Content>
                <View style={styles.address_content}>
                    <View style={{flexDirection: 'row', alignItems: 'center', flex: 3}}>
                        <Button bordered >
                            <Icon name="pin"/>
                        </Button>
                        <View style={{marginLeft: 15}}>
                            <Text style={{fontSize: 15}}>Deliver to Other</Text>
                            <Text style={{fontSize: 11}}>{this.state.choosed_address}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1}}>
                        <TouchableOpacity onPress={() => {this.RBSheet.open()}}><Text note style={{color: '#0e3880', fontSize: 13, textAlign: 'right', width: '100%'}}>{this.state.delivery_address_id ? "CHANGE" : "ADD"}</Text></TouchableOpacity>
                    </View>
                </View>
                <RBSheet
                    ref={ref => { this.RBSheet = ref}}
                    height={350}
                    duration={500}
                >
                    <View style={{backgroundColor: '#dae8ec', justifyContent: 'center', padding: 8}}>
                        <Text style={{fontSize: 18, width: '100%', textAlign: 'center'}}>Choose a delivery address</Text>
                    </View>
                    <ScrollView>
                        {this.state.addresses.map((address, idx) => (
                            <TouchableOpacity onPress={() => this.choose_address(address)}>
                                <View style={{flexDirection: 'row', alignItems: 'center', padding: 8, paddingRight: 15}} key={idx}>
                                    <View style={{flex: 1, alignItems: 'center'}}>
                                        <Icon name="pin" style={{color: "#666666"}}/>
                                    </View>
                                    <View style={{flex: 7, alignItems: 'center'}}>
                                        <Text style={{marginLeft: 10}}>{address.address_1} {address.address_2}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', justifyContent: 'center', flex: 1, alignItems: 'center'}}>
                                        <Button iconLeft small bordered onPress={() => this.removeAddress(address.addresses_id)}><Icon name="trash" style={{marginLeft: 6, marginRight: 6}}/></Button>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                        <View style={{flexDirection: 'row', alignItems: 'center', padding: 8}}>
                            <TouchableOpacity onPress={() => this.new_address()}>
                                <Icon name="add" style={{color: 'green', fontSize: 15}}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.RBSheet.close(); this.setState({addressModal: true})}}>
                                <Text style={{marginLeft: 10, color: 'green'}}>ADD NEW ADDRESS</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </RBSheet>
                <Footer style={{height: 40}}>
                    <FooterTab>
                        <Button onPress={() => this.check_out_handle()}>
                            <Text style={{fontSize: 16}}>PLACE YOUR ORDER</Text>
                        </Button>
                    </FooterTab>
                </Footer>
                <Modal isVisible={this.state.addressModal}>
                    <View style={styles.address_modal}>
                        <Item underline style={{margin: 10, height: 35}}>
                            <Input value={this.state.new_address_1} style={{fontSize: 15}} placeholder="Location" onChange={(event) => this.change_input('new_address_1', event)}/>
                        </Item>
                        <Item underline style={{margin: 10, height: 35}}>
                            <Input value={this.state.new_address_2} style={{fontSize: 15}} placeholder="House/Flat No" onChange={(event) => this.change_input('new_address_2', event)}/>
                        </Item>
                        <View style={{flexDirection: 'row', justifyContent: 'flex-end', margin: 10}}>
                            <Button small onPress={() => this.addAddress()}><Text>Yes</Text></Button>
                            <Button small onPress={() => this.setState({addressModal: false})} danger><Text>No</Text></Button>
                        </View>
                    </View>
                </Modal>
                { this.state.spinner ? <Spinner style={styles.Spinner}/> : <></>}
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    content: {
        padding: 10, marginBottom: 60
    },
    SmallBtn: { width: 24, height: 24, marginRight: 10},
    listTitle: { fontSize: 13, fontWeight: 'bold', marginBottom: 5 },
    listText: { fontSize: 12 },
    price_item: { 
        flex: 1, flexDirection: 'row', justifyContent: 'flex-start', 
        alignItems: "center", paddingTop: 5,
        paddingBottom: 5, paddingRight: 5
    },
    block: {
        paddingLeft: 5, paddingRight: 5, paddingTop: 8, paddingBottom: 8
    },
    block_content: {
        paddingLeft: 10, flexDirection: 'column', alignItems: 'center'
    },
    block_item: {
        width: '100%', flexDirection: 'row', alignItems: 'center', 
        justifyContent: 'flex-start', paddingTop: 5, paddingBottom: 5
    },
    product_name: {
        fontSize: 14, width: '100%'
    },
    address_content: {
        height: 60, padding: 5, borderTopColor: '#cccccc', 
        borderTopWidth: 0.5, position: 'absolute', bottom: 40, 
        width: '100%', flexDirection: 'row', flexWrap: 'wrap',  
        backgroundColor: 'white', justifyContent: 'space-between'
    },
    address_modal: {
        backgroundColor: '#ffffff',
        padding: 10
    },
    Spinner: {
        width: '100%', height: '100%',
        position: 'absolute',
        backgroundColor: '#000000aa'
    }
})
mapStateToProps = (state) => {
    return {
        added_products: state.added_products,
        addresses: state.addresses.addresses
    }
}

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)