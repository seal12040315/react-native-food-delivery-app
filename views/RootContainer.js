import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { fromRight } from 'react-navigation-transitions';
import { Root } from 'native-base'
import Location from './Location'
import Home from './Home'
import Login from './Login'
import Register from './Register'
import Forgot from './Forgot'
import Reset from './Reset'
import Profile from './Profile'
import Restaurant from './Restaurant'
import Product from './Product'
import Checkout from './Checkout'
import OrderStatus from './OrderStatus'
import TrackOrder from './TrackOrder'
import MyAddress from './MyAddress'
import Order from './Order'
import Wallet from './Wallet'
import OrderDetail from './OrderDetail';

const createStackNavigators = () => {
    return createStackNavigator({
        LocationScreen: {
            screen: Location
        },
        HomeScreen: {
            screen: Home
        }, 
        LoginScreen: {
            screen: Login
        }, 
        RegisterScreen: {
            screen: Register
        },
        ProfileScreen: {
            screen: Profile
        },
        ForgotScreen: {
            screen: Forgot
        },
        ResetScreen: {
            screen: Reset
        },
        RestaurantScreen: {
            screen: Restaurant
        },
        CheckoutScreen: {
            screen: Checkout
        },
        ProductScreen: {
            screen: Product
        },
        OrderStatusScreen: {
            screen: OrderStatus
        },
        TrackOrderScreen: {
            screen: TrackOrder
        },
        MyAddressScreen: {
            screen: MyAddress
        },
        OrderScreen: {
            screen: Order
        },
        OrderDetailScreen: {
            screen: OrderDetail
        },
        WalletScreen: {
            screen: Wallet
        }
    }, {
        initialRouteName: 'HomeScreen',       
        headerMode: "none",
        transitionConfig: () => fromRight(500),
    })
}

class RootContainer extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const Navigator = createStackNavigators()
        const AppContainer = createAppContainer(Navigator)
        return (
            <Root>
                <AppContainer/>
            </Root>
        )
    }
}

export default RootContainer

