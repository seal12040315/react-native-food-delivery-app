import React from 'react'
import {View, StyleSheet, AsyncStorage} from 'react-native'
import {Container, Content, Text} from 'native-base'
import MapView, {Marker} from 'react-native-maps'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'

class OrderDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            order_items: [],
            restaurant_name: "",
            order_staus: "",
            order_uid: "",
            order_date: "",
            paid_amount: 0,
            lat: 0, 
            lng: 0
        }
    }
    async componentDidMount() {
        let order = this.props.navigation.getParam("order")
        let location = JSON.parse(await AsyncStorage.getItem('location'))
        this.setState({
            order_status: order.order_status,
            restaurant_name: order.restaurant_name,
            order_date: order.order_date,
            paid_amount: order.paid_amount,
            order_uid: order.order_uid,
            order_items: order.order_items,
            lat: location.lat,
            lng: location.lng
        })
    }
    
    render() {

        return (
            <Container>
                <HeaderComp back="OrderScreen" title={"ORDER #" + this.state.order_uid}/>
                <Content style={styles.content}>
                    {this.state.lat ? 
                    <MapView
                        style={{height: 250}}
                        initialRegion={{
                            latitude: this.state.lat,
                            longitude: this.state.lng,
                            latitudeDelta: 0.5,
                            longitudeDelta: 0.5
                        }}
                    >
                        <Marker coordinate={{latitude: this.state.lat, longitude: this.state.lng}} title={this.state.restaurant_name}/>
                    </MapView> : 
                    <></>}
                    <View style={styles.block}>
                        <View style={styles.order_status}>
                            <Text  style={[styles.listTitle, {width: '100%', textAlign: 'center', fontSize: 16}]}>{`${this.state.order_status} on ${this.state.order_date}`}</Text>
                        </View>
                    </View>
                    <View style={styles.block}>
                        <Text style={styles.listTitle}>BILL DETAILS</Text>
                        {this.state.order_items.map((item, idx) => (
                            <View style={styles.block_item} key={idx}>
                                <Text style={styles.listText}>{item.name}</Text>
                                <Text style={styles.listText}>{item.qty}</Text>
                            </View>
                        ))}
                        <View style={styles.block_item}>
                            <Text style={styles.listText}>Total Paid Price</Text>
                            <Text style={styles.listText}>₹{this.state.paid_amount}</Text>
                        </View>
                    </View>
                </Content>
                <FooterComp/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 10
    },
    order_status: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    listTitle: {
        fontSize: 13,
        fontWeight: 'bold',
        marginBottom: 5
    },
    listText: {
        fontSize: 12
    },
    block: {
        paddingLeft: 5, paddingRight: 5, paddingTop: 8, paddingBottom: 8
    },
    block_content: {
        paddingLeft: 10, flexDirection: 'column', alignItems: 'center'
    },
    block_item: {
        width: '100%', flexDirection: 'row', alignItems: 'center', 
        justifyContent: 'space-between', paddingTop: 5, paddingBottom: 5,
        borderBottomWidth: 0.5,
        borderBottomColor: '#cccccc'
    }
})
export default OrderDetail