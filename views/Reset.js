import React from 'react'
import { StyleSheet, ScrollView, View, Image } from 'react-native'
import { Container, Button, Item, Input, Icon, Text, Toast } from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import HeaderComp from '../components/HeaderComp'
import axios from 'axios'
import { config } from '../config'

class ResetPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            newPassword: "",
            confirmPassword: ""
        }
    }

    async componentDidMount() {
        this.props.setSpinnerData({isShow: true})
    }

    async componentDidMount() {
        this.props.setSpinnerData({isShow: false})
    }

    changeInputHandle = (key, value) => {
        this.setState({
            [key]: value.nativeEvent.text
        })
    }

    changepasswordHandle = async () => {
        if (this.state.newPassword === "") {
            Toast.show({
                text: "You must enter a new password.",
                buttonText: "Ok",
                type: "danger",
                position: "top",
                style: {
                    margin: 10
                }
            })
        } else if (this.state.confirmPassword === "") {
            Toast.show({
                text: "You must enter a confirm password.",
                buttonText: "Ok",
                type: "danger",
                position: "top",
                style: {
                    margin: 10
                }
            })
        } else if (this.state.newPassword !== this.state.confirmPassword) {
            Toast.show({
                text: "The new password must be same with the confirm password.",
                buttonText: "Ok",
                type: "danger",
                position: "top",
                style: {
                    margin: 10
                }
            })
        } else {
            let {result} = await axios.post(`${config.baseUrl}/reset-password`, {password: this.state.newPassword})
            if (result) {
                this.props.navigation.navigate('LoginScreen')
            } else {
                Toast.show({
                    text: "An error occured while changing the password.",
                    buttonText: "Ok",
                    type: "danger",
                    position: "top",
                    style: {
                        margin: 10
                    }
                })
            };
        }
    }

    render() {
        return (
            <Container>
                <HeaderComp back="LoginScreen" title="RESET PASSWORD"/>
                <ScrollView>
                    <View  style={styles.Content}>
                        <Image source={require('../assets/images/change-password.png')} resizeMode="stretch" style={styles.Image}/>
                        <Item style={styles.Input}>
                            <Icon name="key"/>
                            <Input placeholder="New Password" secureTextEntry={true} value={this.state.newPassword} onChange={(event) => this.changeInputHandle('newPassword', event)}/>
                        </Item>
                        <Item style={styles.Input}>
                            <Icon name="eye"/>
                            <Input placeholder="Confirm Password" secureTextEntry={true} value={this.state.confirmPassword} onChange={(event) => this.changeInputHandle('confirmPassword', event)}/>
                        </Item>
                        <View style={styles.ButtonContainer}>
                            <Button iconRight rounded onPress={() => this.changepasswordHandle()}>
                                <Text>CHANGE PASSWORD</Text>
                                <Icon name='lock'></Icon>
                            </Button>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        )
    }
}

styles = StyleSheet.create({
    Content: {
        padding: 20,
        alignItems: 'center'
    },
    Image: {
        width: 120, 
        height: 120, 
        marginTop: '10%'
    },
    Input: {
        marginTop: 10,
        marginBottom: 15
    },
    ButtonContainer: {
        width: '100%', 
        alignItems: 'flex-end',
        marginTop: 10
    }
})

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSpinnerData: Actions.setSpinnerData
    }, dispatch)
}

export default connect(null, mapDispatchToProps)(ResetPassword)