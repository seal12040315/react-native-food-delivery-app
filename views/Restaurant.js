import React from 'react'
import { AsyncStorage, StyleSheet, TouchableOpacity } from 'react-native'
import * as Actions from '../redux/actions/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Text, Left, Body, List, ListItem, Thumbnail } from 'native-base'
import { Rating } from 'react-native-ratings'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import axios from 'axios';
import { config } from '../config'
class Restaurant extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            store_type_id: 1,
            lat: "",
            lng: "",
            adertise_type: "New",
            restaurants: [],
            slider: ["http://demo.we4host.com/helloorders/storage/slider-images/9efc2e3d77ee8b1dcd06a0a357e9fe0c.jpg", "http://demo.we4host.com/helloorders/storage/slider-images/9efc2e3d77ee8b1dcd06a0a357e9fe0c.jpg"],
            store_name: ""
        }
    }
    

    async componentDidMount() {
        let store_type = this.props.navigation.getParam('store_type');
        let { store_type_id, name } = store_type;
        let location = await AsyncStorage.getItem('location');
        location = JSON.parse(location)
        let data = {
            store_type_id: store_type_id,
            lat: location.lat,
            lng: location.lng,
            loc: location.loc.split(",")[0],
        }
        this.setState(data);
        this.setState({
            store_name: name
        })
        this.getRestaurantList(data);
    }

    getRestaurantList = async (params) => {
        params = {
            "lat": "14.4322515",
            "lng": "79.97529580000003",
            "loc": "Nellore Golagamudi",
            "store_type_id": "1"
        }
        let res = await axios.post(`${config.baseUrl}/restaurants`, params)
        let slider = this.state.slider
        this.setState({
            restaurants: res.data.response.restaurants ? res.data.response.restaurants: [],
            slider: res.data.response.slider_images.length ? res.data.response.slider_images.map(i => i.image) : slider
        })
    }

    goToProduct = (item) => {
        if (!item.closed_now) {
            this.props.navigation.navigate('ProductScreen', {"restaurant": item})
        }
    }

    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={() => {
                this._carousel.scrrollToIndex(index)
            }}>
                <Image source={{uri: item}}/>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <Container>
                <HeaderComp back="HomeScreen" title={this.state.store_name.toLocaleUpperCase() + " LIST"}/>
                <Content>
                    {/* <SliderBox images={this.state.slider} sliderBoxHeight={180} style={{width: 100}}/> */}
                    <List style={{borderBottomWidth: 0}}>
                    {this.state.restaurants.length ? this.state.restaurants.map((restaurant, index) => (
                        <ListItem style={{borderBottomWidth: 0}} key={index} thumbnail onPress={() => this.goToProduct(restaurant)}>
                            <Left>
                                {restaurant.closed_now ? 
                                    <React.Fragment>
                                        <Thumbnail style={[styles.resThumbnail, {tintColor: '#cccccc'}]} source={{uri: restaurant.image}}/>
                                        <Thumbnail style={[styles.resThumbnail, {position: 'absolute', opacity: 0.3}]} source={{uri: restaurant.image}}/>                        
                                    </React.Fragment> : <Thumbnail style={styles.resThumbnail} source={{uri: restaurant.image}}/>}
                            </Left>
                            <Body style={styles.resDescription}>
                                <Text style={[styles.listText, restaurant.closed_now ? {color: '#cccccc'} : {color: '#000000'}]}>{restaurant.name}</Text>
                                {restaurant.couponinfo ? <Text note style={styles.listSubText}>{`\u2022 ${restaurant.couponinfo}`}</Text> : <></>}
                                <Text style={styles.listSubText} note>{`\u2022 Min ₹${restaurant.min_order} \u2022 ${parseInt(restaurant.delivery_time)} minutes`}</Text>
                                {restaurant.closed_now ? <Text note style={styles.closeRes}>Opens next at {restaurant.next_open_at} {restaurant.next_open_day.toLowerCase()}</Text> : <></>}
                                <Rating
                                    startingValue={restaurant.rating}
                                    ratingCount={5}
                                    readonly
                                    imageSize={13}
                                    style={{paddingVertical: 10}}
                                />
                            </Body>
                        </ListItem>
                    )) : <></>}
                    </List>
                </Content>
                <FooterComp/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    resThumbnail: {
        borderRadius: 5, 
        width: 70, 
        height: 70
    },
    resDescription: {
        paddingBottom: 6, 
        paddingTop: 5, 
        borderBottomWidth: 0, 
        alignItems: 'flex-start'
    },
    listText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    listSubText: {
        fontSize: 12
    },
    closeRes: {
        color: 'red',
        fontSize: 12
    },
    carousel: {
        height: 200
    }
})

mapStateToProps = (state) => {
    return {
        restaurants: state.restaurants
    }
}

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setRestaurantsData: Actions.setRestaurantsData,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Restaurant)