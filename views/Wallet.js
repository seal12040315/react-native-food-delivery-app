import React from 'react'
import { AsyncStorage, StyleSheet, View } from 'react-native'
import { Container, Content, Text } from 'native-base'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import { config } from '../config'
import Axios from 'axios'

class Wallet extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            transactions: {}
        }
    }

    componentDidMount() {
        this.init_wallet();
    }

    init_wallet = async () => {
        let user = JSON.parse(await AsyncStorage.getItem('user'));
        if (user) {
            Axios.post(`${config.baseUrl}/my_wallet_transactions`, {user_id: user.user_id}).then(res => {

                this.setState({
                    transactions: res.data.response
                })
            }, err => {
                alert(err);
            })
        } else {
            this.props.navigation.navigate('LocationScreen');
        }
    }
    set_type(type) {
        if (type == "D" || type == "P") {
            return 1;
        } else {
            return 0;
        }
    }

    render() {
        return (
            <Container>
                <HeaderComp back="HomeScreen" title="WALLET TRANSACTION"/>
                <Content>
                    <View style={styles.header}>
                        <Text note>Your Wallet Balance</Text>
                        <Text style={styles.wallet}>₹{this.state.transactions.wallet}</Text>
                    </View>
                    <View style={styles.history_container}>
                        {Object.keys(this.state.transactions).length ? this.state.transactions.history.map((history, idx) => (
                            <View style={styles.history_content} key={idx}>
                                <View style={{marginBottom: 2, flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start'}}>
                                    <Text style={[styles.history_amount, this.set_type(history.type) ? {color: 'red'} : {color: 'green'} ]}>₹{history.amount}</Text>
                                    <Text style={styles.history_date}>{history.created_date}</Text>
                                </View>
                                <Text style={styles.history_title}>{history.purpose}</Text>
                                <Text style={styles.history_comment}>{history.comment}</Text>
                                <View style={{flex: 1, width: '100%', alignItems: 'flex-end'}}>
                                    <Text style={styles.history_status}>{history.status}</Text>
                                </View>
                            </View>
                        )) : <></>}
                    </View>
                </Content>
                <FooterComp/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        padding: 10,
        flex: 1,
        alignItems: 'center',
        borderBottomWidth: 0.5,
        backgroundColor: '#eeeeee',
        borderBottomColor: '#cccccc'
    },
    wallet: {
        fontSize: 30
    },
    history_container: {
    },
    history_content: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#cccccc',
        padding: 10
    },
    history_amount: {
        fontSize: 16,
        color: 'red',
        marginRight: 10
    },
    history_date: {
        fontSize: 11,
        color: '#888888'
    },
    history_title: {
        fontSize: 18,
        marginBottom: 2
    },
    history_comment: {
        fontSize: 12,
        color: '#888888',
    },
    history_status: {
        fontSize: 12
    }
})

export default Wallet