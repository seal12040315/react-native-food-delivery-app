import React from 'react'
import { StyleSheet, ImageBackground, Image, View, Alert } from 'react-native'
import { Container, Content, Item, Input, Icon, Button, Footer, FooterTab, Text } from 'native-base'
import Axios from 'axios';
import { config } from '../config' 

class Forgot extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ""
        }
    }
    changeInput(key, event) {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    forgotHandle() {
        let params = {
            username: this.state.email
        }
        Axios.post(`${config.baseUrl}/forgot_password`, params).then(res => {
            let {data} = res;
            if (data.status) {
                Alert.alert('Success!', data.message);
            } else {
                Alert.alert('Warnning!', data.message);
            }
        })
    }
    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground source={require('../assets/images/background.png')} resizeMode="stretch" style={styles.bgImage}>
                        <Image source={require('../assets/images/logo.png')} style={styles.logo}/>
                    </ImageBackground>
                    <View style={styles.formContainer}>
                        <Item style={styles.inputContainer}>
                            <Icon name="mail"/>
                            <Input value={this.state.password} placeholder="Email *" onChange={(event) => this.changeInput('email', event)}/>
                        </Item>
                        <View style={styles.buttonContainer}>
                            <Button large rounded warning style={styles.loginButton} onPress={() => this.forgotHandle()}>
                                <Icon name="arrow-round-forward"/>
                            </Button>
                        </View>
                    </View>
                </Content>
                <Footer style={styles.transFooter}>
                    <FooterTab style={styles.transFooter}>
                        <Button transparent style={styles.loginToButton} onPress={() => this.props.navigation.navigate('RegisterScreen')}>
                            <Text style={{fontSize: 15}}>LOGIN</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bgImage: {
        width: '100%',
        height: 260,
    },
    logo: {
        width: 125,
        height: 120,
        marginTop: 65,
        marginLeft: 100
    },
    formContainer: {
        padding: 15
    }, 
    inputContainer: {
        padding: 8,
        height: 45,
        marginTop: 15,
        marginBottom: 15
    },
    buttonContainer: {
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    loginButton: {
        height: 70,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    transFooter: {
        backgroundColor: 'transparent'
    },
    loginToButton: {
        alignItems: 'flex-end'
    }
})

export default Forgot