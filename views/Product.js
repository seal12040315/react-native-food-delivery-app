import React from 'react'
import { StyleSheet, ImageBackground, View} from 'react-native'
import { Container, Text, Icon, Title, Button, Content, Footer, FooterTab, Spinner, Tabs, Tab, TabHeading, ScrollableTab, List, ListItem, Badge } from 'native-base'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../redux/actions'
import Axios from 'axios';
import { config } from '../config'
import HeaderComp from '../components/HeaderComp'
import NumericInput from 'react-native-numeric-input'

class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            restaurant: {},
            categories: [],
            reviews: [],
            spinner: false,
            added_products: []
        }
    }

    async componentDidMount() {
        let restaurant = this.props.navigation.getParam('restaurant')
        await this.setState({
            spinner: true,
            restaurant: restaurant
        })
        this.props.chooseRestaurantData(restaurant)
        let {data} = await Axios.post(`${config.baseUrl}/menu`, {restaurant_id: this.state.restaurant.restaurant_id});
        this.setState({
            categories: data.response.categories,
            reviews: data.response.reviews,
        })
        this.setState({spinner: false})
    }

    add_product = (product, value) => {
        let added_products = [...this.state.added_products]
        let isExist = added_products.findIndex(item => item.item_id === product.item_id)
        if (isExist !== -1) {
            added_products[isExist].qty = value;
            added_products[isExist].id = added_products[isExist].item_id
        } else {
            product.qty = value;
            product.id = product.item_id
            added_products.push(product)
        }
        this.setState({
            added_products: added_products
        });
    }

    getQtys = () => {
        let items = 0;
        this.state.added_products.forEach(item => {
            items += item.qty
        })
        return items
    }

    calcTotalPrice = () => {
        let total_price = 0;
        this.state.added_products.forEach((item) => {
            total_price += parseInt(item.price) * item.qty;
        })
        return total_price
    }

    goToCheckout = () => {
        this.props.setAddedProductsData(this.state.added_products);
        this.props.navigation.navigate('CheckoutScreen', { "restaurant": this.state.restaurant})
    }

    render() {
        return (
            <Container>
                <HeaderComp back="RestaurantScreen" title="PRODUCT LIST"/>
                <Content>
                    <ImageBackground source={{uri: this.state.restaurant.image}} style={styles.banner} resizeMode="stretch">
                        <View style={styles.subBanner}>
                            <Title style={styles.bannerTitle}>{this.state.restaurant.name}</Title>
                            <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', width: '100%'}}>
                                <Button transparent small vertical>
                                    <Icon name="calendar" style={styles.bannerIcon}/>
                                    <Text style={styles.bannerText}>Min ₹{this.state.restaurant.min_order}$ Order</Text>
                                </Button>
                                <Button transparent small vertical>
                                    <Icon name="map" style={styles.bannerIcon}/>
                                    <Text style={styles.bannerText}>{this.state.restaurant.delivery_time}</Text>
                                </Button>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={styles.tabContainer}>
                        <Tabs renderTabBar={()=> <ScrollableTab style={{height: 40}}/>} tabBarUnderlineStyle={{borderBottomWidth: 1, borderBottomColor: '#2a2d86'}}>
                        { this.state.categories.map((category, idx1) => (
                            <Tab key={idx1} heading={<TabHeading style={{height: 40, backgroundColor: 'white'}}><Text style={{fontSize: 14, color: '#2a2d86'}}>{category.name}</Text></TabHeading>}>
                                <List>
                                {category.items.map((item, idx2) => (
                                    <ListItem style={{paddingTop: 8, marginLeft: 5, paddingBottom: 8}} key={idx2}>
                                        <View style={{flex: 5}}>
                                            <Text style={{fontSize: 14, width: '100%'}}>{item.name}</Text>
                                        </View>
                                        <View style={{flex: 2}}>
                                            <Text note>₹{item.price}</Text>
                                        </View>
                                        <View style={{flex: 1}}>
                                            <NumericInput onChange={value => this.add_product(item, value)} iconSize={12} totalWidth={54} totalHeight={20} minValue={0} rounded={true}/>
                                        </View>
                                    </ListItem>
                                ))}
                                </List>
                            </Tab>
                        ))}
                        </Tabs>
                    </View>
                </Content>
                <Footer style={{height: 40}}>
                    <FooterTab>
                        <Button badge vertical>
                            <Badge style={{fontSize: 6}}><Text>{this.getQtys()}</Text></Badge>
                            <Text style={{color: '#ffffff', fontSize: 17, fontWeight: 'bold'}}>₹{this.calcTotalPrice()}</Text>
                        </Button>            
                        <Button vertical onPress={() => this.goToCheckout()}>
                            <Icon name="cart"/>
                        </Button>                
                    </FooterTab>
                </Footer>
                { this.state.spinner ? <Spinner style={styles.Spinner}/> : <></> }
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    label: {
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 10
    },
    Spinner: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: '#000000aa'
    },
    banner: {
        width: '100%', height: 150
    },
    subBanner: {
        backgroundColor: '#00000077', 
        width: '100%', 
        height: '100%', 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    bannerIcon: {
        color: '#ffffff'
    },
    bannerTitle: {
        paddingBottom: 15, 
        fontSize: 20, 
        borderBottomColor: '#ffffff', 
        width: '80%', 
        borderBottomWidth: 2
    },
    bannerText: {
        fontSize: 10,
        color: '#ffffff'
    },
    tabContainer: {
        paddingTop: 0, 
        paddingBottom: 10, 
    },
    SmallBtn: {
        width: 60,
        height: 25
    }
})

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setAddedProductsData: Actions.setAddedProductsData,
        chooseRestaurantData: Actions.chooseRestaurantData
    }, dispatch)
}
export default connect(null, mapDispatchToProps)(Product)