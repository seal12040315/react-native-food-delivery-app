import React from 'react'
import { StyleSheet, View, AsyncStorage, Dimensions, TouchableOpacity, Image, FlatList } from 'react-native'
import * as Actions from '../redux/actions/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Container, Content, Card, CardItem, Spinner, Body, Text, Icon, ListItem, Left, Thumbnail } from 'native-base'
import axios from 'axios';
import { config } from '../config'
import { Rating } from 'react-native-ratings';
import FooterComp from '../components/FooterComp'
import Modal from 'react-native-modal'
import LocationModal from '../components/LocationModal'

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            spinner: false,
            categories: [],
            activeCategories: [],
            recommends: [],
            activeRecommends: [],
            categoryState: 0,
            recommendState: 0,
            offers: [],
            offerState: 0,
            activeOffers: [],
            location: "",
            locationModal: false,
        }
    }
 
    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.init_data() 
        });
    }

    init_data = async () => {
        this.setState({spinner: true})
        let user = await AsyncStorage.getItem('user')
        if (user !== null) {
            let location = await AsyncStorage.getItem('location');
            location = JSON.parse(location);
            
            this.setState({location: location.loc.split(",")[0]})
            let categories = await axios.post(`${config.baseUrl}/store_types`, {})
            this.props.setCategoriesData(categories.data.response)     
            let restaurants = await axios.post(`${config.baseUrl}/restaurants`, {
                store_type_id: 1, lat: location.lat,
                lng: location.lng, loc: location.loc.split(",")[0]
            });
            let activeCategories = categories.data.response.reverse().slice(0, 6);
            restaurants = restaurants.data.response.restaurants ? restaurants.data.response.restaurants : []
            let recommends = restaurants.filter(i => i.rating >= 4);
            let activeRecommends = recommends.slice(0, 2);
            let offers = restaurants.filter(i => i.couponcode !== null);
            let activeOffers = offers.slice(0, 4);
            this.setState({
                categories: categories.data.response,
                activeCategories: activeCategories,
                recommends: recommends,
                activeRecommends: activeRecommends,
                offers: offers,
                activeOffers: activeOffers,
                spinner: false
            })
            user = JSON.parse(user)
            let result = await axios.post(`${config.baseUrl}/my_addresses`, {user_id: user.user_id});
            this.props.setAddresses(result.data.response)
        } else {
            this.props.navigation.navigate('LocationScreen')
        }
    }

    closeModal = (location) => {
        this.setState({
            locationModal: false,
            location: location
        })
    }

    categoryExpand = (active) => {
        if (active) {
            this.setState(state => ({
                categoryState: active,
                activeCategories: state.categories 
            }))
        } else {
            this.setState(state => ({
                categoryState: active,
                activeCategories: state.categories.slice(0, 4)
            }))
        }
    }

    recommendExpand = (active) => {
        if (active) {
            this.setState(state => ({
                recommendState: active,
                activeRecommends: state.recommends
            }))
        } else {
            this.setState(state => ({
                recommendState: active,
                activeRecommends: state.recommends.slice(0, 2)
            }))
        }
    }

    offerExpand = (active) => {
        if (active) {
            this.setState(state => ({
                offerState: active,
                activeOffers: state.offers
            }))
        } else {
            this.setState(state => ({
                offerState: active,
                activeOffers: state.offers.slice(0, 4)
            }))
        }
    }

    goToProduct = (item) => {
        if (!item.closed_now) {
            this.props.navigation.navigate('ProductScreen', {"restaurant": item})
        }
    }

    render() {
        return (
            <Container>
                <Content style={{padding: 10}}>
                    <TouchableOpacity onPress={() => this.setState({locationModal: true})}>
                        <View style={styles.locationTopbar}>
                            <Icon name="search" style={styles.locationTopbarIcon}/>
                            <Text style={styles.locationTopbarText}>{this.state.location}</Text>
                            <Icon name="create" style={[styles.locationTopbarIcon, styles.locationTopbarRightIcon]}/>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.Label}>
                        <Text note>Explore Nearby</Text>
                        <TouchableOpacity onPress={() => this.categoryExpand(!this.state.categoryState)}>
                            <Text note style={styles.viewAll}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.activeCategories.length ? 
                        <FlatList
                            data={this.state.activeCategories}
                            renderItem={ ({item}) => (
                                <TouchableOpacity underlayColor="white" style={{marginLeft: 4, marginRight: 4}} onPress={() => this.props.navigation.navigate('RestaurantScreen', {'store_type': item})}>
                                    <Card style={{width: Dimensions.get('screen').width/3 - 18}} transparent>
                                        <CardItem cardBody style={{margin: 5}}>
                                            <Image source={{uri: item.icon}} style={{width: '100%', height: 60, borderRadius: 8}} resizeMode="stretch"/>
                                        </CardItem>
                                        <CardItem style={{ paddingBottom: 5, paddingTop: 0}}>
                                            <Body style={{alignItems: 'center'}}>
                                                <Text style={[styles.listText]}>{item.name}</Text>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            )}
                            keyExtractor={({item, index}) => index}
                            horizontal={false}
                            numColumns={3}
                        ></FlatList> : 
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', height: 80, paddingLeft: '12%', paddingRight: '12%'}}>
                            <Text style={{color: 'red', textAlign: 'center'}}>We can't find the categories.</Text>
                        </View>
                    }
                    <View style={styles.Label}>
                        <Text note>Recommended for you</Text>
                        <TouchableOpacity onPress={() => this.recommendExpand(!this.state.recommendState)}>
                            <Text note style={styles.viewAll}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.activeRecommends.length ?
                        <FlatList
                            data={this.state.activeRecommends}
                            style={{marginLeft: 0, paddingLeft: 0}}
                            renderItem={ ({item}) => (
                                <TouchableOpacity onPress={() => this.goToProduct(item)}>
                                    <Card style={{width: Dimensions.get('screen').width/2 - 20, marginLeft: 5, marginRight: 5}} transparent>
                                        <CardItem cardBody>
                                            {item.closed_now ? 
                                                <React.Fragment>
                                                    <Image source={{uri: item.image}} style={[styles.recommenedIcon, {tintColor: '#cccccc'}]} resizeMode="stretch"/>
                                                    <Image source={{uri: item.image}} style={[styles.recommenedIcon, {position: 'absolute', opacity: 0.4}]} resizeMode="stretch"/>
                                                </React.Fragment> : <Image source={{uri: item.image}} style={styles.recommenedIcon} resizeMode="stretch"/>}
                                        </CardItem>
                                        <CardItem style={{paddingTop: 5, paddingBottom: 5}}>
                                            <Body>
                                                <Text style={[styles.listText, item.closed_now ? {color: '#555555'} : {color: 'cccccc'} ]}>{item.name}</Text>
                                                {item.couponcode ? <Text style={styles.listSubText}>{`\u2022 ${item.couponinfo}`}</Text> : <></>}
                                                <Text style={styles.listSubText}>{`\u2022 Min ₹${item.min_order} \u2022 ${parseInt(item.delivery_time)} minutes`}</Text>
                                                {item.closed_now ? <Text note style={styles.closeRes}>{ `\u2022 Open time: ${item.next_open_at} ${item.next_open_day.toLowerCase()}`}</Text> : <></>}
                                                <View style={{flexDirection: 'row', alignItems:'center'}}>
                                                    <Rating
                                                        startingValue={item.rating}
                                                        ratingCount={5}
                                                        readonly
                                                        imageSize={11}
                                                        style={{paddingVertical: 11, marginRight: 8}}
                                                    />
                                                    <Text note style={styles.listSubText}><Text style={{fontWeight: 'bold', fontSize: 12}}>4.0 </Text></Text>
                                                </View>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            )}
                            keyExtractor={({item, index}) => index}
                            horizontal={false}
                            numColumns={2}
                        ></FlatList> :
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', height: 80, paddingLeft: '12%', paddingRight: '12%'}}>
                            <Text style={{color: 'red', textAlign: 'center'}}>We can't find the restaurants recommended based on that location.</Text>
                        </View>
                    }
                    
                    <View style={styles.Label}>
                        <Text note>Offers</Text>
                        <TouchableOpacity onPress={() => this.offerExpand(!this.state.offerState)}>
                            <Text note style={styles.viewAll}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    {
                        this.state.activeOffers.length ? this.state.activeOffers.map((item, index) => (
                            <ListItem style={{borderBottomWidth: 0}} key={index} thumbnail onPress={() => this.goToProduct(item)}>
                                <Left>
                                    {item.closed_now ? 
                                        <React.Fragment>
                                            <Thumbnail square source={{uri: item.image}} style={{borderRadius: 5, tintColor: '#cccccc'}}/>
                                            <Thumbnail square source={{uri: item.image}} style={{borderRadius: 5, position: "absolute", opacity: 0.3}}/>
                                        </React.Fragment> : <Thumbnail square source={{uri: item.image}} style={{borderRadius: 5}}/>}
                                </Left>
                                <Body style={{borderBottomWidth: 0, paddingBottom: 12, paddingTop: 12}}>
                                    <Text style={[styles.listText, item.closed_now ? {color: '#cccccc'} : {color: '#000000'}]}>{item.name}</Text>
                                    {item.couponcode ? <Text style={styles.listSubText}>{`\u2022 ${item.couponinfo}`}</Text> : <></>}
                                    <Text style={styles.listSubText}>{`\u2022 Min ₹${item.min_order} \u2022 ${parseInt(item.delivery_time)} minutes`}</Text>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <Rating
                                            startingValue={item.rating}
                                            ratingCount={5}
                                            readonly
                                            imageSize={12}
                                            style={{paddingVertical: 10, marginRight: 8}}
                                        />
                                        <Text note style={styles.listSubText}><Text style={{fontSize: 11}}>4.0 </Text></Text>
                                    </View>
                                </Body>
                            </ListItem>
                        )) :
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', height: 80, paddingLeft: '12%', paddingRight: '12%'}}>
                            <Text style={{color: 'red', textAlign: 'center'}}>We can't find the restaurants offered based on that location.</Text>
                        </View>
                    }
                </Content>
                <FooterComp/>
                <Modal isVisible={this.state.locationModal} style={{margin: 15}}>
                    <View style={{backgroundColor: '#ffffff', padding: 10}}>
                        <LocationModal closeModal={this.closeModal} init_data={this.init_data}/>
                    </View>
                </Modal>
                { this.state.spinner ? <Spinner style={styles.Spinner}/> : <></>}
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    Label: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginBottom: 10, 
        marginTop: 10
    },
    Spinner: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: '#000000aa'
    },
    locationTopbarIcon: {
        flex: 1, fontSize: 20, color: '#555555'
    },
    listText: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    listSubText: {
        fontSize: 10,
    },
    listBody: {
        paddingTop: 0,
        paddingBottom: 0,
        margin: 0
    },
    viewAll: {
        color: '#3F51B5', 
        fontSize: 12
    },
    locationTopbar: {
        flexDirection: 'row', 
        color: '#cccccc', 
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 5, 
        paddingBottom: 5, 
        paddingLeft: 10, 
        paddingRight: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc'
    },
    locationTopbarText: {
        flex: 12
    },
    locationTopbarRightIcon: {
        justifyContent: 'flex-end'
    },
    recommenedIcon: {
        width: '100%', 
        height: 110, 
        borderRadius: 5
    },
    closeRes: {
        color: 'red',
        fontSize: 10,
    }
})
mapStateToProps = (state) => {
    return {
        user: state.user,
        categories: state.categories
    }
}

mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setCategoriesData: Actions.setCategoriesData,
        setAddresses: Actions.setAddresses
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
