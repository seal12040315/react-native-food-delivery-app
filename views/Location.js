import React from 'react'
import { PermissionsAndroid, Platform, AsyncStorage, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { Container, Content, Icon, Card, CardItem } from 'native-base'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import FooterComp from '../components/FooterComp'
import Geolocation from '@react-native-community/geolocation'
import Geocoder from 'react-native-geocoder'
class Location extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        recentLocations: []
      }
    }
    async componentDidMount() {
      let recentLocations = await AsyncStorage.getItem('recentLocations');
      recentLocations = JSON.parse(recentLocations);
      this.setState({
        recentLocations: recentLocations ? recentLocations : []
      })
    }
    before_current_location = () => {
      if (Platform.OS === 'android') {
        this.requestLocationPermission();
      } else {
        this.current_location();
      }
    }
    async requestLocationPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title' : 'Location Permission',
            'message': 'MyMapApp needs access to your location'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.current_location();
        } else {
          alert('Location permission denied')
        }
      } catch(err) {
        alert(err)
      }
    }
    current_location = () => {
      Geolocation.getCurrentPosition((position) => {
          Geocoder.geocodePosition({lat: position.coords.latitude, lng: position.coords.longitude}).then(async (res) => {
              let location = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                  loc: res[0].streetName ? res[0].streetName : res[0].formattedAddress
              };
              await AsyncStorage.setItem('location', JSON.stringify(location));
              await this.setRecentLocation(location);
              this.props.navigation.navigate('LoginScreen');
          })
        }, err => {
            alert(JSON.stringify(err))
        }, { enableHighAccuracy: true, timeout: 3000, maximumAge: 1000 }
      )
    }
    
    setLocation = async (details) => {
      let location = {
        lat: details.geometry.location.lat,
        lng: details.geometry.location.lng,
        loc: details.formatted_address
      }
      await AsyncStorage.setItem('location', JSON.stringify(location))
      await this.setRecentLocation(location)
      this.props.navigation.navigate('LoginScreen')
    }
    setOriginLocation = async (location) => {
      await AsyncStorage.setItem('location', JSON.stringify(location));
      await this.setRecentLocation(location);
      this.props.navigation.navigate('LoginScreen')
    }

    setRecentLocation = async (location) => {
      let recentLocations = await AsyncStorage.getItem('recentLocation');
      recentLocations = recentLocations !== null ? JSON.parse(recentLocations) : [];
      if (recentLocations.length < 3) {
        recentLocations.unshift(location);
      } else {
        recentLocations.pop();
        recentLocations.unshift(location)
      }
      await AsyncStorage.setItem('recentLocations', JSON.stringify(recentLocations));
    }

    render() {
      let query = {key: 'AIzaSyCtlR4RO7TRNLFw0RjiXQncr9Nzu3cXbIk', language: 'en', types: 'geocode'}
      let geoLocation = {
        textInputContainer: {
          width: '100%',
          backgroundColor: '#ffffff',
          borderTopWidth: 1,
          borderRightWidth: 1,
          borderLeftWidth: 1,
          borderRadius: 10,
          borderColor: '#cccccc',
          borderTopColor: '#cccccc'
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }
      return (
        <Container>
          <Content style={{padding: 10}}>
            <GooglePlacesAutocomplete
              placeholder='Please enter location' minLength={2} autoFocus={false}
              returnKeyType={'search'} listViewDisplayed='auto'
              fetchDetails={true} renderDescription={row => row.description}
              onPress={(data, details = null) => this.setLocation(details)}
              query={query} styles={geoLocation} currentLocation={false}
              currentLocationLabel="Current location" nearbyPlacesAPI='GooglePlacesSearch'
              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} debounce={200} 
              renderLeftButton={() => <Icon name="search" style={{paddingTop: 12, paddingLeft: 12, color: '#555555', fontSize: 20}}/>}
            >
              {this.state.recentLocations.length ? <Text>Recent Location</Text> : <></>}
              <Card transparent>
                {this.state.recentLocations.length ? this.state.recentLocations.map((location, idx) => (
                  <TouchableOpacity key={idx} onPress={() => {this.setOriginLocation(location)}}>
                    <CardItem>
                      <Icon name="locate" style={styles.locateIcon}></Icon>
                      <Text style={styles.locateText}>{ location.loc !== null ? location.loc.split(',')[0] : ''}</Text>
                    </CardItem>
                  </TouchableOpacity>
                )) : <></>}
              </Card>
            </GooglePlacesAutocomplete>
          </Content>
          <FooterComp/>
        </Container>
      )
    }
    
}

const styles = StyleSheet.create({
  locateIcon: {fontSize: 20,color: '#ec5175'},
  locateText: {color: '#ec5175'}
})
        
export default Location