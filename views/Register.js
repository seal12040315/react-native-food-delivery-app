import React from 'react'
import { StyleSheet, ImageBackground, Image, View, Alert, TouchableOpacity } from 'react-native'
import { Container, Content, Item, Input, Icon, Button, Footer, Text } from 'native-base'
import { Col, Grid } from 'react-native-easy-grid'
import Axios from 'axios';
import { config } from '../config' 

class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            mobile: "",
            email: "",
            password: "",
            confirm: ""
        }
    }

    changeInput(key, event) {
        this.setState({
            [key]: event.nativeEvent.text
        })
    }

    registerHandle() {
        let params = {
            name: this.state.name,
            email: this.state.email,
            mobile: this.state.mobile,
            referral: false,
            password: this.state.password,
            mobile_verified: false
        }
        Axios.post(`${config.baseUrl}/register`, params).then(res => {
            let { data } = res;
            if (data.status) {
                this.props.navigation.navigate('LoginScreen')
            } else {
                Alert.alert('Warnning!', data.message)
            }
        }, err => {
            alert(JSON.stringify(err))
        })
    }
    render() {
        return (
            <Container>
                <Content>
                    <ImageBackground source={require('../assets/images/background.png')} resizeMode="stretch" style={styles.bgImage}>
                        <Image source={require('../assets/images/logo.png')} style={styles.logo}/>
                    </ImageBackground>
                    <View style={styles.formContainer}>
                        <Text style={styles.Title}>Sign Up</Text>
                        <Text style={styles.subTitle}>Create your free account here</Text>
                        <Grid>
                            <Col style={{paddingRight: 5}}>
                                <Item style={styles.inputContainer}>
                                    <Input value={this.state.name} placeholder="Name *" onChange={(event) => this.changeInput('name', event)}/>
                                </Item>
                            </Col>
                            <Col style={{paddingLeft: 5}}>
                                <Item style={styles.inputContainer}>
                                    <Input value={this.state.email} placeholder="Email *" onChange={(event) => this.changeInput('email', event)}/>
                                </Item>
                            </Col>
                        </Grid>
                        <Item style={styles.inputContainer}>
                            <Input value={this.state.mobile} placeholder="Mobile *" onChange={(event) => this.changeInput('mobile', event)}/>
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input value={this.state.password} placeholder="Password *" onChange={(event) => this.changeInput('password', event)}/>
                        </Item>
                        <Item style={styles.inputContainer}>
                            <Input value={this.state.confirm} placeholder="Confirm *" onChange={(event) => this.changeInput('confirm', event)}/>
                        </Item>
                        <View style={styles.buttonContainer}>
                            <Button large rounded primary style={styles.loginButton} onPress={() => this.registerHandle()}>
                                <Icon name="arrow-round-forward"/>
                            </Button>
                        </View>
                    </View>
                </Content>
                <Footer style={[styles.transFooter, styles.footerTab]}>
                        <Text style={styles.footerText}>Already Have Account?</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
                            <Text style={styles.signinButton}>SIGN IN</Text>
                        </TouchableOpacity>
                </Footer>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bgImage: {
        width: '100%',
        height: 250,
    },
    logo: {
        width: 125,
        height: 120,
        marginTop: 65,
        marginLeft: 100
    },
    formContainer: {
        padding: 15,
        paddingTop: 0
    },
    Title: {
        fontSize: 30, 
        fontWeight: 'bold', 
        paddingLeft: 10
    },
    subTitle: {
      paddingLeft: 10,
      marginBottom: 10
    },
    inputContainer: {
        padding: 8,
        height: 32,
        marginTop: 6,
        marginBottom: 6
    },
    buttonContainer: {
        marginTop: 8,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    loginButton: {
        height: 70,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    transFooter: {
        height: 40,
        backgroundColor: 'transparent'
    },
    footerTab: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    footerText: {
        fontSize: 15
    },
    signinButton: {
        color: '#3F51B5'
    }
})

export default Register