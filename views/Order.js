import React from 'react'
import {StyleSheet, AsyncStorage} from 'react-native'
import {Container, Left, Body, Thumbnail, List, ListItem, Button, Text, Content, Card, CardItem} from 'native-base'
import Modal from 'react-native-modal'
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import Axios from 'axios';
import {config} from '../config'

class Order extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            activeOrder: {},
            detailModal: false,
        }
    }

    async componentDidMount() {
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        let params = {
            user_id: user.user_id
        }
        Axios.post(`${config.baseUrl}/my_orders`, params).then(res => {
            this.setState({
                orders: res.data.response
            })
        }, err => {
            alert(JSON.stringify(err))
        })
    }

    orderDetail(order) {
        this.props.navigation.navigate('OrderDetailScreen', {order: order});
    }

    render() {
        return (
            <Container>
                <HeaderComp back="HomeScreen" title="MY ORDERS"/>
                <Content>
                    <List style={{borderBottomWidth: 0}}>
                    {this.state.orders.length ? this.state.orders.map((order, index) => (
                        <ListItem style={{borderBottomWidth: 0}} key={index} thumbnail onPress={() => this.orderDetail(order)}>
                            <Left>
                                <Thumbnail style={styles.orderThumbnail} source={{uri: order.image}}/>
                            </Left>
                            <Body style={styles.orderDescription}>
                                <Text style={styles.listText}>{order.restaurant_name}</Text>
                                <Text note style={styles.listSubText}>{`\u2022 Order No: ${order.order_uid}`}</Text>
                                <Text note>{`\u2022 Order Date: ${order.order_date}`}</Text>                                 
                                <Text note>{`\u2022 Amount: ${order.paid_amount}`}</Text>
                                <Text note>{`\u2022 Status: ${order.order_status}`}</Text>
                            </Body>
                        </ListItem>
                    )) : <></>}
                    </List>
                </Content>
                <FooterComp/>
                <Modal isVisible={this.state.detailModal}>
                    <Card>
                        <CardItem header bordered>
                            <Text>{this.state.activeOrder.restaurant_name} ({this.state.activeOrder.order_uid})</Text>
                        </CardItem>
                        {
                            this.state.activeOrder.order_items ?
                            this.state.activeOrder.order_items.map((item, idx) => (
                                <CardItem key={idx}>
                                    <Body>
                                        <Text>{`${item.name} \u2022 ${item.qty}`}</Text>
                                    </Body>
                                </CardItem>
                            )) : <></>
                        }
                        <CardItem footer style={{justifyContent: 'flex-end'}}>
                            <Button onPress={() => this.setState({detailModal: false})} primary small><Text>Close</Text></Button>
                        </CardItem>
                    </Card>
                </Modal>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    orderThumbnail: {
        borderRadius: 5, 
        width: 80, 
        height: 80
    },
    orderDescription: {
        paddingBottom: 6, 
        paddingTop: 5, 
        borderBottomWidth: 0, 
        alignItems: 'flex-start'
    },
    listText: {
        fontSize: 15
    },
    listSubText: {
        fontSize: 12
    }
})

export default Order