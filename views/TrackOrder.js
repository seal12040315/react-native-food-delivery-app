import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Container, Content, Item, Input, Text, Button } from 'native-base' 
import HeaderComp from '../components/HeaderComp'
import FooterComp from '../components/FooterComp'
import Axios from 'axios';
import { config } from '../config'

class TrackOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            order_uid: "",
            mobile: "",
        }
    }

    changeInput = (key, event) => {
        this.setState({
            [key] : event.nativeEvent.text
        })
    }

    trackHandle = () => {
        let params = {
            order_uid: this.state.order_uid,
            mobile: this.state.mobile
        }
        Axios.post(`${config.baseUrl}/track_order`, params).then(res => {
            if (res.data.status) {
                this.props.navigation.navigate('OrderStatusScreen', {order_uid: res.data.response.order_uid, step: 1})
            } else {
                alert(res.data.message)
            }
        })
    }

    render() {
        return (
            <Container>
                <HeaderComp title="TRACT ORDER" back="HomeScreen"/>
                <Content style={{padding: 15}}>
                    <View style={styles.subHeader}>
                        <Text style={styles.title}>Check Your Order Status</Text>
                        <Text style={{textAlign: 'center'}}>
                            Please enter your order number and order mobile Number
                            Number to know the status of your order
                        </Text>
                    </View>
                    <View>
                        <Item>
                            <Input placeholder="Order Id" value={this.state.order_id} onChange={(event) => this.changeInput('order_uid', event)}/>
                        </Item>
                        <Item>
                            <Input placeholder="Mobile Number" value={this.state.mobile} onChange={(event) => this.changeInput('mobile', event)}/>
                        </Item>
                        <Button style={{marginTop: 15}} onPress={() => this.trackHandle()}><Text style={{textAlign: 'center', width: '100%'}}>Check status</Text></Button>
                    </View>
                </Content>
                <FooterComp/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    subHeader: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
        marginBottom: 10    
    }
})
export default TrackOrder

